import {useState} from 'react';

import {Row, Col, Card, Button} from "react-bootstrap";


// export default function CourseCard(props){
//     // check to see if was successfully passed
//     console.log(props);
// }

export default function CouresCard({courseProp}) {
    
    // console.log(typeof props)

        const {name, description, price} = courseProp;

        // hooks is built from react
        // react hooks - useState -> store its state
        // count --> is the getter for the data
        // setCount --> is the settle the data from the count 
         // usesStae(0) --> is the initial the value which as to store tha start from zero 
        const [count, setCount] = useState(10);   
        const [studentEnroll, setCountenEnroll] = useState(0);  
        
        function enroll (){ 
            if(count > 0){
                setCount(count - 1)
                setCountenEnroll(studentEnroll + 1)
            }else if(count <  10) {
                    alert("No more Slot Available");
            }
          
        }

    

     




        return(

            <Row className="mt-3 mb-3">
                     <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                        <Card.Body>
                            <Card.Title className="fw-bold">{name} </Card.Title>
                            <Card.Subtitle> Course  Description: </Card.Subtitle>      
                            <Card.Text>{description} </Card.Text>
                            <Card.Subtitle>Course Price</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Text>Enrolles:{studentEnroll}</Card.Text>
                            <Card.Text>Seats:{count}</Card.Text>
                            <Button variant="primary" onClick={enroll}>Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
                </Row>

        )
}